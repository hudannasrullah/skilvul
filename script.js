// Tambahkan kode JavaScript kalian di file ini
function getFormData(){
     let firstName = document.getElementById("#first-name").value;
     let lastName = document.getElementById("#last-name").value;
     let city = document.getElementById("#city").value;
     let zipCode = document.getElementById("#zip-code").value;
     let check = document.getElementById("#check").checked;

    let objek = {
        firstName : firstName,
        lastName : lastName,
        city : city,
        zipCode : zipCode,
        check : check
    };
    return objek;

}